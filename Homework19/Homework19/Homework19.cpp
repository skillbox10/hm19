﻿#include <iostream>
#include<string>

class Animal  {

protected:
    std::string phrase = "Error I Geuss";

public:

 
    Animal(){}



    virtual void Voice() = 0;

};

class Dog : public Animal {
private:
    std::string line = "Bark";

public:

    Dog() {}

    void Voice() override {
        std::cout << line << "\n";
    }

};
class Cat : public Animal {
private:
    std::string line = "Mow";

public:

    Cat() {}

    void Voice() override {
        std::cout << line << "\n";
    }

};

int main()
{
    int num = 2;
    Animal** animals = new Animal*[num];
    Dog dog;
    Cat cat;
    
    animals[0] = new Dog;
    animals[1] = new Cat;
    
       
    for (int i = 0; i < num; i++)
    {

        animals[i]->Voice();

    }
}
